// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування - для вставки спецсимволів
// Які засоби оголошення функцій ви знаєте?  declaration і expression.
// Що таке hoisting, як він працює для змінних та функцій? процес доступу до змінних до їх оголошення.



function createNewUser(
    userFirstName = prompt('Enter you name: ',''),
    userLastName = prompt('Enter you second name',''),
    userBirthday = prompt('Enter you birthday, format dd.mm.yyyy ','')
) {
    
    this.firstName = userFirstName;

    this.lastName = userLastName;

    this.birthday = new Date(
        userBirthday.slice(6, 10),
        userBirthday.slice(3, 5) - 1,
        userBirthday.slice(0, 2)
    );

    this.getBirthday = function () {
        return this.birthday.toLocaleDateString();
    };

    this.getAge = function () {
       return (
        Math.floor(
            (new Date().getTime() - new Date (this.birthday)) / (365 * 24 * 3600 * 1000)) + "years" 
        );
           
    };
    
    this.getLogin = function(){
        let newLogin = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    };

    this.getPassword = function () {
        return (
            this.firstName.charAt(0).toUpperCase() + 
            this.lastName.toLowerCase() + 
            this.birthday.getFullYear()
        );
    };
};

let newUser = new createNewUser();

console.log(newUser);

console.log(`Your Age is: ${newUser.getAge()}`);

console.log(`Your Password is: ${newUser.getPassword()}`);

console.log(`Your login is: ${newUser.getLogin()}`);

console.log(newUser.firstName);
console.log(newUser.lastName);


